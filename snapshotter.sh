#!/bin/bash
set -o nounset -o errexit -o pipefail
shopt -s dotglob # Needed for attachments starting with a dot
ALBS_PARALLELIZE_DOWNLOAD="${ALBS_PARALLELIZE_DOWNLOAD:-false}"
ALBS_DOWNLOAD_WAIT="${ALBS_DOWNLOAD_WAIT:-0.05}"
ALBS_RANGE_DOWNLOAD_ENABLED="${ALBS_RANGE_DOWNLOAD_ENABLED:-false}"
ALBS_RANGE_DOWNLOAD_CHUNK="${ALBS_RANGE_DOWNLOAD_CHUNK:-$(date +%-d)}"
ALBS_RANGE_DOWNLOAD_CHUNKS="${ALBS_RANGE_DOWNLOAD_CHUNKS:-20}"
ALBS_RANGE_DOWNLOAD_CHUNK="$((ALBS_RANGE_DOWNLOAD_CHUNK % ALBS_RANGE_DOWNLOAD_CHUNKS + 1))"

function wget() {
  local user_agent
  user_agent="albs ($(git rev-parse --short HEAD); https://gitlab.archlinux.org/archlinux/archlinux-bugs-snapshotter)"
  command wget --max-redirect 0 \
               --user-agent="${user_agent}" \
               --no-verbose "${@}"
}

function get_newest_task_id() {
  wget --output-document=- "https://bugs.archlinux.org/index.php?project=0&status[]=&changedfrom=2021-04-01" | grep --perl-regexp --only-matching --max-count 1 "(?<=task)[0-9]+"
}

function generate_urls() {
  eval "echo https://bugs.archlinux.org/task/{$1..$2} | tr ' ' '\n'"
}

function _download() {
  set -o nounset -o errexit -o pipefail

  if [[ ${ALBS_PARALLELIZE_DOWNLOAD} = true ]]; then
    local dir
    dir="$(grep --only-matching "[0-9]*$" <<< "${1}")"
    mkdir "${dir}"
    cd "${dir}"
  fi

  wget --recursive \
       --include-directories="user,themes,ajax,javascript" \
       --page-requisites \
       --convert-links \
       --adjust-extension \
       --span-hosts \
       --no-host-directories \
       --domains=archlinux.org,ajax.googleapis.com \
       --wait="${ALBS_DOWNLOAD_WAIT}" \
       --input-file=<(printf "%s\n" "${@}") || [[ ${?} = 8 ]] # 8 Server issued an error response.
}

function download() {
  echo "Tasks download started: $(date -Isec)" >> metadata
  if [[ ${ALBS_PARALLELIZE_DOWNLOAD} != true ]]; then
    mapfile -t array <<< "${1}"
    _download "${array[@]}"
  else
    export ALBS_DOWNLOAD_WAIT
    export -f wget _download
    time echo "${1}" | xargs --max-procs=5 --max-args=200 bash -c "_download \"\${@}\"" bash
    mkdir final
    for d in */; do
      if [[ ${d} = final/ ]]; then
        continue
      fi
      rsync --recursive --ignore-existing "${d}" final/
      rm -rf "${d}"
    done
    mv final/* .
    rmdir final
  fi
  echo "Tasks download finished: $(date -Isec)" >> metadata
}

function download_attachment() {
  local filename=""
  if ! grep --quiet --max-count=1 "^${1} ${2} " attachments/attachments.txt; then
    rm -fr "attachments/${1}/tmp"
    mkdir -p "attachments/${1}/"{tmp,"${2}"}
    cd "attachments/${1}/tmp"

    options=(--content-disposition)
    if [[ ${3:-false} = true ]]; then
      options=(--output-document=attachment)
    fi
    wget "${options[@]}" \
         "https://bugs.archlinux.org/index.php?getfile=${2}" || { [[ ${?} = 3 ]] && echo "Error downloading attachment: ${2}" && cd .. && rmdir tmp "${2}" && cd ../../ || exit 1 && [[ ${3:-false} = true ]] && return; download_attachment "${1}" "${2}" "true"; return; }
    local files=(*)
    filename="${files[0]}"

    cd ..
    mv "tmp/${filename}" "${2}"
    rmdir tmp
    cd ../../
  else
    filename="$(grep --max-count=1 "^${1} ${2} " attachments/attachments.txt | cut -f3- -d " ")"
  fi

  # https://unix.stackexchange.com/a/33005
  # shellcheck disable=SC2001
  escaped_filename="$(sed 's/[\#&]/\\&/g' <<< "${filename}")"
  sed --regexp-extended --in-place "s#(href=\")https?://bugs.archlinux.org/(task/[0-9]+|)\?getfile=${2}(\")#\1../attachments/${1}/${2}/${escaped_filename}\3#" "task/${1}.html"
  echo "${1} ${2} ${filename}" >> attachments/attachments.txt
}

function download_attachments() {
  echo "Attachments download started: $(date -Isec)" >> metadata
  mkdir -p attachments
  if [[ ! -e attachments/attachments.txt ]]; then
    echo "# Task id | Attachment id | Filename" > attachments/attachments.txt
  fi

  { grep --extended-regexp --recursive --only-matching "href=\"https?://bugs.archlinux.org/(task/[0-9]+|)\?getfile=[0-9]+\"" || true; } | sed --regexp-extended 's/task\/([0-9]+).*getfile=([0-9]+)"/\1 \2/' | sort --numeric-sort | uniq | while read -r line; do
    task_id="${line% *}"
    attachment_id="${line#* }"
    download_attachment "${task_id}" "${attachment_id}"
    sleep "${ALBS_DOWNLOAD_WAIT}"
  done
  sort --numeric-sort attachments/attachments.txt | uniq > attachments/attachments.txt.tmp
  mv attachments/attachments.txt{.tmp,}
  echo "Attachments download finished: $(date -Isec)" >> metadata
}

function cleanup_html() {
  time find . -path ./attachments -prune -false -o -name '*.html' | xargs --max-procs=50 -I {} bash -c "xsltproc --output '{}.new' --html \"${1}/transform.xsl\" '{}' && mv '{}.new' '{}'" 2> >(sed '/ID comment[0-9]* already defined/,+2 d')
}

function prettify() {
  time find . -path ./attachments -prune -false -o -name '*.html' | xargs --max-procs="$(nproc)" --max-args=100 prettier --write
}

function main() {
  local newest_task_id
  if [[ -n ${1:-} && ${1} != -1 ]]; then
    newest_task_id="${1}"
  else
    newest_task_id="$(get_newest_task_id)"
  fi
  local min=1
  local max=${newest_task_id}

  if [[ ${ALBS_RANGE_DOWNLOAD_ENABLED} = true ]]; then
    local max_tasks="$(( newest_task_id / ALBS_RANGE_DOWNLOAD_CHUNKS ))"
    local overlap="$(( max_tasks * 5 / 100))" # 5% overlap

    min="$(( (ALBS_RANGE_DOWNLOAD_CHUNK - 1) * max_tasks))"
    max="$((ALBS_RANGE_DOWNLOAD_CHUNK * max_tasks + overlap))"
    if (( newest_task_id < max )); then
      max="${newest_task_id}"
    fi
  fi

  local urls
  urls="$(generate_urls "${min}" "${max}")"
  local orig_pwd="${PWD}"
  local snapshot_dir=""
  if [[ -z ${4:-} ]]; then
    snapshot_dir="snapshots/$(date --iso-8601=minute)"
    mkdir -p "${snapshot_dir}"
  else
    snapshot_dir="${4}"
  fi
  cd "${snapshot_dir}"
  echo "ALBS commit: $(git show --no-patch --format="%h (\"%s\")")" > metadata
  echo "${min}-${max}" > range
  download "${urls}"
  cleanup_html "${orig_pwd}"
  if [[ ${2:-true} = true ]]; then
    download_attachments
  fi
  if [[ ${3:-true} = true ]]; then
    prettify
  fi
}

# $1 - newest_task_id
# $2 - download attachments (true or false)
# $3 - prettify the HTML files (true or false)
# $4 - snapshot_dir
main "${@}"
